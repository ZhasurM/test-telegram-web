FROM python:3.7.6 AS builder

WORKDIR /usr/src/app/

COPY requirements.txt .

RUN pip install --no-cache-dir -r requirements.txt

COPY . /usr/src/app/

EXPOSE 5000

CMD ["python", "wsgi.py"]
