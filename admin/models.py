from database import Base, session
from sqlalchemy import Column, ForeignKey, Table
from sqlalchemy.orm import relationship, backref
from sqlalchemy.types import String, Integer, Float


# User - пользователи из телеграм


class User(Base):
	__abstract__ = True


class Admin(User):

	__tablename__ = 'admins'

	id = Column(Integer(), primary_key=True)
	name = Column(String())

	def __init__(self, name: str):
		self.name = name 


class Tg_User(User):

	__tablename__ = 'tg_users'

	id = Column(Integer(), primary_key=True)
	username = Column(String())
	first_name = Column(String())

	def __init__(self, id: int, username: str, first_name: str):
		self.id = id
		self.username = username
		self.first_name = first_name


class Product(Base):

	__tablename__ = 'products'

	id = Column(Integer(), primary_key=True)
	category = Column(Integer(), ForeignKey('category.id'))
	name = Column(String())
	description = Column(String())
	price = Column(Float())
	image = Column(String())


	def __init__(
		self, 
		name: str, 
		category: int, 
		description: str, 
		price: float, 
		image: str
		):
		self.name = name
		self.category = category
		self.description = description
		self.price = price
		self.image = image


class Category(Base):

	__tablename__ = 'category'

	id = Column(Integer(), primary_key=True)
	name = Column(String(), unique=True)
	products_list = relationship("Product")

	def __init__(self, name):
		self.name = name

	def get_products(self) -> list:
		return self.products_list


	@staticmethod
	def get_all_category():
		return session.query(Category).all()


# cart_products = Table(
# 	"cart_products",
# 	Base.metadata,
# 	Column("cart_id", ForeignKey("carts.id")),
# 	Column("product_id", ForeignKey("products.id"))
# )


# class Cart(Base):

# 	__tablename__ = 'carts'

# 	id = Column(Integer(), primary_key=True)
# 	user_id = Column(Integer(), ForeignKey('tg_users.id'))
# 	owner = relationship("Tg_User", back_populates="cart")
# 	products_list = relationship("Product", secondary=cart_products, backref=backref("in_carts", lazy="dynamic"))
# 	summ = Column(Float)

# 	def __init__(self, user_id: int, owner: Tg_User) -> None:
# 		self.user_id = user_id
# 		self.owner = owner
# 		self.summ = 0

# 	def add_product(self, product: Product) -> None:
# 		self.summ = self.summ + product.price
# 		self.products_list.append(product)


# 	def remove_product(self, product: Product) -> None:	 
# 		if product in self.products_list:
# 			self.summ = self.summ - product.price
# 			self.products_list.remove(product)

	
# 	def get_products(self) -> list:
# 		return self.products_list

	
# 	def get_cart_summ(self) -> float:
# 		return self.summ


	

	







