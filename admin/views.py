from crypt import methods
from http.client import HTTPResponse
import os
# from http.client import HttpResponse
from flask import Blueprint, render_template, request, redirect, url_for, jsonify, make_response, Response
from database import Base, session
from admin.models import Admin, Tg_User, Product, Category
from mytools import allowed_file
from werkzeug.utils import secure_filename
from database import BASEDIR
from myform import ProductForm, ProductFormEdit
from log import logger


admin = Blueprint('admin', __name__, template_folder='templates', static_folder='static')


@admin.route('/admin')
def index():


	return render_template('admin/dashboard/index.html')


@admin.route('/pay')
def pay():
	return render_template('admin/dashboard/pay.html')


@admin.route('/products')
def products():
	print("krtrkjtnkrjn")
	products_list = session.query(Product).all()
	category_list = session.query(Category).all()

	return render_template('admin/product/index.html', products_list=products_list, category_list=category_list)


@admin.route('/add_category', methods=['GET', 'POST'])
def add_category():
	if request.method == 'POST':
		name = request.form.get('name')
		session.add(Category(name))
		session.commit()
		return redirect(url_for('admin.products'))
	return render_template('admin/product/add_category.html')


@admin.route('/delete_product/<product_id>', methods=['GET'])
def delete_product(product_id):
	session.query(Product).filter(Product.id == int(product_id)).delete()
	return redirect(url_for('admin.products'))



@admin.route('/edit_product/<pr_id>', methods=['GET', 'POST'])
def edit_product(pr_id):
	
	product = session.query(Product).filter(Product.id == int(pr_id)).first()

	form = ProductFormEdit()

	form.category.choices = [(cat.id, cat.name) for cat in session.query(Category).all()]
	form.category.default = product.category

	if request.method == 'POST':
		name = form.name.data
		price = form.price.data
		description = form.description.data
		image = form.image.data
		imagename = ''
		if image and allowed_file(image.filename):
			imagename = secure_filename(image.filename)
			image.save(os.path.join(BASEDIR, imagename))
		category = form.category.data

		new_data = {
			Product.name: name, Product.category: int(category),
			Product.description: description, Product.price: price, 
			Product.image: imagename
		}
		for key, value in list(new_data.items()):
			if value == '':
				del new_data[key]
		print(new_data)


		session.query(Product).filter(Product.id == int(pr_id)).update(new_data)
		session.commit()

		return redirect(url_for('admin.products'))
	return render_template('admin/product/edit_product.html', form=form, product=product)


@admin.route('/pay_ok', methods=['POST'])
def pay_ok():
	#text = request.get_json()
	print(request.form)
	print("ok")
	return "success", 200


@admin.route('/pay_fail/<invoiceId>', methods=['POST'])
def pay_fail(invoiceId):
	print("bad", invoiceId)
	return "success", 200


@admin.route('/get_categories', methods=['GET'])
def get_categories():

	categories = session.query(Category).all()

	#respns = jsonify({'code': 200, 'data': categories}).headers.add('Access-Control-Allow-Origin', '*')

	# Преобразование в список словаре
	categories_list = [category.__dict__ for category in categories]
	# Удаление служебного атрибута _sa_instance_state
	logger.info(categories_list)
	category_dict = {}
	for category_dict in categories_list:
		category_dict.pop('_sa_instance_state', None)

	logger.info(categories_list)
	resp = make_response(categories_list)
	resp.headers.add('Access-Control-Allow-Origin', '*')

	return resp

@admin.route('/get_products', methods=['GET'])
def get_products():

	products = session.query(Product).all()

	# Преобразование в список словаре
	products_list = [product.__dict__ for product in products]
	# Удаление служебного атрибута _sa_instance_state
	for products_dict in products_list:
		products_dict.pop('_sa_instance_state', None)

	resp = make_response(products_list)
	resp.headers.add('Access-Control-Allow-Origin', '*')

	return resp

@admin.route('/add_product', methods=['GET', 'POST'])
def add_product():
	
	form = ProductForm()

	form.category.choices = [(cat.id, cat.name) for cat in session.query(Category).all()]

	if request.method == 'POST':

		name = form.name.data
		price = form.price.data
		description = form.description.data
		image = form.image.data
		if image and allowed_file(image.filename):
			imagename = secure_filename(image.filename)
			image.save(os.path.join(BASEDIR, imagename))
		else:
			imagename = 'default.png'
		category = form.category.data

		session.add(Product(name, int(category), description, price, imagename))
		session.commit()

		return redirect(url_for('admin.products'))
	
	return render_template('admin/product/add_product.html', form=form)

