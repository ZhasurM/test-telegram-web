from database import session
from uuid import uuid4
from admin.models import Category, Product
import os


from telegram import (
    Update,
    InlineQueryResultArticle,
    InputTextMessageContent,
)
from telegram.ext import (
    ContextTypes,
)


def get_product_list(qr):

    product = session.query(Category).filter(Category.name == qr).first()

    if product:
        result = [InlineQueryResultArticle(
            id=str(uuid4()), 
            title=prod.name,
            description=str(prod.price),
            thumb_url=os.getenv('PHOTO_URL')+prod.image,
            input_message_content=InputTextMessageContent(
                message_text=prod.name+'h7yf83f8gfrv098owpow0e08u')
            ) for prod in product.products_list]
        return result
    else:
        result = [InlineQueryResultArticle(
            id=str(uuid4()), 
            title=prod.name,
            description=str(prod.price),
            thumb_url=os.getenv('PHOTO_URL')+prod.image,
            input_message_content=InputTextMessageContent(
                message_text=prod.name+'h7yf83f8gfrv098owpow0e08u')
            ) for prod in session.query(Product).all()]
        return result


async def inline_query_1(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Handle the inline query. This is run when you type: @botusername <query>"""
    query = update.inline_query.query
    await update.inline_query.answer(get_product_list(query))
        


