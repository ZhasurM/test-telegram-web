from database import session
from admin.models import Category, Tg_User
from log import logger
from main.keyboards import category_list, calculate_summ
from telegram import (
    ReplyKeyboardMarkup, 
    ReplyKeyboardRemove,
    Update, 
    InlineKeyboardButton, 
    InlineKeyboardMarkup
    )
    

from telegram.constants import ParseMode
from admin.models import Category, Product


#from config import TOKEN
from telegram.ext import (
    ContextTypes,
    ConversationHandler,
    MessageHandler,
    filters,
    CallbackQueryHandler
)

(
    START_ROUTES,
    STOP_ROUTES
) = range(2)
(
    CONFIRM_NUMBER,
    DELIVERY_METHOD,
    ADDRESS,
    DETAILS,
    CONV_END
) = range(5)


async def start_order(update: Update, context: ContextTypes.DEFAULT_TYPE):
    query = update.callback_query
    context.user_data["order"] = {}
    await query.answer()
    await query.edit_message_text("Введите номер телефона\n*формат 81231234567\n*на ваш номер будет отправлен код подтверждения")
    return CONFIRM_NUMBER

async def confirm_number(update: Update, context: ContextTypes.DEFAULT_TYPE):
    context.user_data["una_phone"] = update.message.text
    # генерируем код и отправляем сообщение на номер
    # Здесь проверка номера

    await update.message.reply_text(
        "Введите код из сообщения \nтест 1234"
    )
    return DELIVERY_METHOD


async def delivery_method(update: Update, context: ContextTypes.DEFAULT_TYPE):
    if update.message.text != '1234':
        await update.message.reply_text(
        "Код не совпадает"
        )
        return DELIVERY_METHOD
    else:
        context.user_data["order"]["phone"] = context.user_data["una_phone"]
        keyboard = [["Самовывоз", "Доставка"]]
        reply_markup = ReplyKeyboardMarkup(keyboard)
        await update.message.reply_text("Выберите способ получения:", reply_markup=reply_markup)
        return ADDRESS


async def address(update: Update, context: ContextTypes.DEFAULT_TYPE):

    context.user_data["order"]["delivery_method"] = update.message.text

    if update.message.text == "Самомовывоз":
        await update.message.reply_text("Введите аддрес доставки:")
    else:
        # branchs = session.query(Branch).all()
        branchs = ["Омск, пр. Мира 11 а", "Степногорск, ул. Абая 15"]
        if len(branchs) < 1:
            await update.message.reply_text(f"Адрес нашего заведения:\n {branchs}")
        else:
            keyboard = [branchs]
            reply_markup = ReplyKeyboardMarkup(keyboard, one_time_keyboard=True, resize_keyboard=True)

            await update.message.reply_text("Выберите :", reply_markup=reply_markup)
    return DETAILS

    

# async def delivery_address(update: Update, context: ContextTypes.DEFAULT_TYPE):
#     pass
    


async def details(update: Update, context: ContextTypes.DEFAULT_TYPE):

    await update.message.reply_text(
        "--",
        reply_markup=ReplyKeyboardRemove(),
    )

    reply_markup=ReplyKeyboardRemove()
    
    context.user_data["order"]["address"] = update.message.text

    keyboard = [[InlineKeyboardButton("Редактировать корзину", callback_data="CART")]]
    reply_markup = InlineKeyboardMarkup(keyboard)

    text = f"""
    Ваша корзина на сумму {int(calculate_summ(context.user_data["cart"]))} ₸
    """
    for prod, count in context.user_data["cart"].items():
        text = text + f"\n{prod}: {count} шт." 

    order = context.user_data["order"]
    text2 = f"""
    Детали заказа\nНомер для связи - {order["phone"]}\nСпособ получения - {order["delivery_method"]}\nАдрес - {order["address"]}
    """
    keyboard1 = [[InlineKeyboardButton("Редактировать детали", callback_data="ORDER")]]
    reply_markup1 = InlineKeyboardMarkup(keyboard1)

    await update.message.reply_text(text, reply_markup=reply_markup)
    await update.message.reply_text(text2, reply_markup=reply_markup1)
    return ConversationHandler.END
    

async def end_conversation(update: Update, context: ContextTypes.DEFAULT_TYPE):
    return ConversationHandler.END



conv_handler_order = ConversationHandler(
    
    entry_points = [CallbackQueryHandler(start_order, pattern="^" + str("ORDER") + "$")],
    states={
        CONFIRM_NUMBER: [MessageHandler(filters.TEXT, confirm_number)],
        DELIVERY_METHOD: [MessageHandler(filters.TEXT, delivery_method)],
        ADDRESS: [MessageHandler(filters.TEXT, address)],
        DETAILS: [MessageHandler(filters.TEXT, details)]
    },
    allow_reentry = True,
    fallbacks=[MessageHandler(filters.Regex("^Назад$"), end_conversation)],
)