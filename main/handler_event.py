from telegram import (
    ReplyKeyboardMarkup, 
    ReplyKeyboardRemove, 
    Update,
    )
from telegram.ext import (
    CommandHandler,
    ContextTypes,
    ConversationHandler,
    MessageHandler,
    filters,
)


(
    NAME, 
    TELEPHONE, 
    DATE, 
    GUESTS, 
    FORMAT, 
    DURATION, 
    LOCATION, 
    KITCHEN, 
    BUDGET,
    POZHELANIYA,
    COMPLITE
) = range(11)


async def order(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Start the conversation and asks the user their name."""
    
    await update.message.reply_text(
        "Как Вас зовут?",
        reply_markup=ReplyKeyboardMarkup(
            [[update.message.from_user.first_name]], 
            resize_keyboard=True,
            one_time_keyboard=True
        ),
    )
    return NAME


async def name(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Stores the name and asks tel."""
    text = update.message.text
    context.user_data["name"] = text
    await update.message.reply_text(
        "Введите, пожалуйста, номер телефона для связи",
        reply_markup=ReplyKeyboardRemove(),
    )

    return TELEPHONE


async def telephone(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Stores the telephone and asks for a date."""
    text = update.message.text
    context.user_data["telephone"] = text
    await update.message.reply_text(
        "Введите дату проведения мероприятия"
        "\n*например, 25 сентябрь 2022",
        reply_markup=ReplyKeyboardRemove(),
    )

    return DATE


async def ivent_date(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Stores the date and asks for a guests."""
    text = update.message.text
    context.user_data["date"] = text
    await update.message.reply_text(
        "Предпологаемое количество гостей",
        reply_markup=ReplyKeyboardRemove(),
    )

    return GUESTS


async def guests(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Store the guests and ask for a ivent format"""
    text = update.message.text
    context.user_data["guests"] = text
    await update.message.reply_text(
        "Формат мероприятия (нужное подчеркнуть,"
        " а лучше подробно описать: банкет, фуршет,"
        " выездное обслуживание, только доставка,"
        " свадьба, юбилей, корпоратив, День рождения и т.д.)"
    )
    return FORMAT


async def event_format(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Stores the guests and asks for a format."""
    text = update.message.text
    context.user_data["format"] = text
    await update.message.reply_text(
        "Продолжительность праздника в часах. Чтобы"
        " понимать, какое количество блюд мы можем предложить."
        "\n *нужно ввести число",
        reply_markup=ReplyKeyboardRemove(),
    )

    return DURATION


async def duration(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Stores the format and asks for a duration."""
    text = update.message.text
    context.user_data["duration"] = text
    await update.message.reply_text(
        "Локация мероприятия или адрес доставки блюд.",
        reply_markup=ReplyKeyboardRemove(),
    )

    return LOCATION


async def location(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Stores the duration and asks for a location."""
    text = update.message.text
    context.user_data["location"] = text
    await update.message.reply_text(
        "Наличие кухни на площадке, если нет — мы привезём с собой.",
        reply_markup=ReplyKeyboardMarkup(
            [["Есть", "Нет"]], 
            resize_keyboard=True, 
            one_time_keyboard=True
        ),
    )

    return KITCHEN


async def kitchen(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Stores the location and asks for a kitchen."""
    text = update.message.text
    context.user_data["kitchen"] = text
    await update.message.reply_text(
        "Примерный бюджет на персону."
        "\n*в сумах",
        reply_markup=ReplyKeyboardRemove(),
    )

    return BUDGET


async def budget(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Stores the kitchen and asks for a budget."""
    text = update.message.text
    context.user_data["budget"] = text
    await update.message.reply_text(
        "Ваши пожелания: Ваши пожелания: возможно, кто-то из гостей вегетарианец, или не ест определенные продукты, пишите все, что нужно знать о вас и ваших гостях."
    )

    return POZHELANIYA


async def pozhelaniya(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Stores the budget and asks for a pozhelaniya."""
    ans = update.message.text
    context.user_data["pozhelaniya"] = ans
    text = context.user_data
    dt = """
    <strong>Ваша заявка сохранена, мы свяжемся с вами в ближайшее время.</strong>
    -----------------------------------------------------
    <b>Детали заказа</b>
    Имя: {}
    Номер: {}
    Дата проведения: {}
    Количество предпологаемых гослей: {}
    Формат мероприятия: {}
    Продолжительность: {} часов
    Место проведения: {}
    Наличие кухни: {}
    Примерный бюджет на персону: {} сумов
    Пожелания: {}
    """.format(
        text["name"], 
        text["telephone"],
        text["date"],
        text["guests"],
        text["format"],
        text["duration"],
        text["location"],
        text["kitchen"],
        text["budget"],
        text["pozhelaniya"]
        )


    await update.message.reply_text(dt, parse_mode='html', reply_markup=main_markup)

    return ConversationHandler.END


async def complit_hand(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Stores the pozhelaniay and complite handler"""
    text = context.user_data
    await update.message.reply_text(
        reply_markup=main_markup,
    )

    return ConversationHandler.END

conv_handler_event = ConversationHandler(
    entry_points=[MessageHandler(filters.Regex("^Сделать заказ$"), order)],
    states={
        NAME: [MessageHandler(filters.TEXT, name)],
        TELEPHONE: [MessageHandler(filters.TEXT, telephone)],
        DATE: [MessageHandler(filters.TEXT, ivent_date)],
        GUESTS: [MessageHandler(filters.TEXT, guests)],
        FORMAT: [MessageHandler(filters.TEXT, event_format)],
        DURATION: [MessageHandler(filters.TEXT, duration)],
        LOCATION: [MessageHandler(filters.TEXT, location)],
        KITCHEN: [MessageHandler(filters.TEXT, kitchen)],
        BUDGET: [MessageHandler(filters.TEXT, budget)],
        POZHELANIYA: [MessageHandler(filters.TEXT, pozhelaniya)],
        COMPLITE: [MessageHandler(filters.TEXT, complit_hand)]
    },
    fallbacks=[CommandHandler("cancel", complit_hand)],
)