from admin.models import Category, Product
from database import session
from telegram import (
    ReplyKeyboardMarkup,
    InlineKeyboardButton, 
    InlineKeyboardMarkup
    )

(
    INCREASE,
    DECREASE,
    CONFIRM,
    END_PRODUCT
) = range(4)

main_keyboard = [
    [InlineKeyboardButton("📖 Наше меню", callback_data="category")],
    [InlineKeyboardButton("☎️ Связаться с нами", callback_data="feedback"), InlineKeyboardButton("🗳 Оставить отзыв", callback_data="review")]
    ]

def count_keybord(count: int) -> InlineKeyboardMarkup:
    keybord = [
        [
            InlineKeyboardButton("-", callback_data=str(DECREASE)), 
            InlineKeyboardButton(str(count), callback_data=str(count)), 
            InlineKeyboardButton("+", callback_data=str(INCREASE))
        ],
        [
            InlineKeyboardButton('Добавить в корзину', callback_data=str(CONFIRM))
        ],
        [
            InlineKeyboardButton('< Назад', callback_data=str(CONFIRM))
        ]
    ]
    return InlineKeyboardMarkup(keybord)


def calculate_summ(cart: list) -> float:

    summ = 0

    for prod, count in cart.items():
        print(prod, count)
        price = session.query(Product).filter(Product.name == prod).first().price
        summ = summ + (price * count)

    return summ


def category_list(cart: dict) -> list:
    
    keyboard = [[InlineKeyboardButton("🛒Корзина: " + str(calculate_summ(cart)), callback_data="CART")]]

    category_list1 = []

    cn = len(session.query(Category).all())

    for i in range(0, cn, 2):
        if i == cn-1:
            category_list1.append(InlineKeyboardButton(
                                session.query(Category).all()[i].name, 
                                switch_inline_query_current_chat=session.query(Category).all()[i].name))
            keyboard.append(category_list1)
            category_list1 = []
            break
        category_list1.append(InlineKeyboardButton(
                                session.query(Category).all()[i].name, 
                                switch_inline_query_current_chat=session.query(Category).all()[i].name))
        category_list1.append(InlineKeyboardButton(
                                session.query(Category).all()[i+1].name, 
                                switch_inline_query_current_chat=session.query(Category).all()[i+1].name))
        keyboard.append(category_list1)
        category_list1 = []

    keyboard.append([InlineKeyboardButton("< Назад", callback_data="main_menu")])

    return keyboard


def cart_keyboard(cart: dict) -> list:

    keyboard = [[InlineKeyboardButton("🛒Корзина: " + str(int(calculate_summ(cart))), callback_data="1")]]

    for name, count in cart.items():
        keyboard.append([InlineKeyboardButton(
                            str(name) + ": " + str(count), 
                            callback_data=str("h7yf83f8gfrv098owpow0e08u"+name)),
                        InlineKeyboardButton(
                            str('❌'),
                            callback_data=str("uhuhuhu_"+name)
                        )])

    keyboard.append([InlineKeyboardButton(
                        "✅Оформить заказ", 
                        callback_data="ORDER"),
                    InlineKeyboardButton(
                        "Очистить карзину",
                        callback_data="RESET"
                    )])
    keyboard.append([InlineKeyboardButton(
                        "< Назад", 
                        callback_data="category")])
    return keyboard




