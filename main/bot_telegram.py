import os
#from main.cart_edit import conv_handler_cart

from itertools import groupby
from main.handler_event import conv_handler_event
from main.my_inline_query import inline_query_1
from database import session
from main.order import conv_handler_order
from main.keyboards import (
    category_list,
    calculate_summ,
    cart_keyboard,
    main_keyboard,
    )
from admin.models import Category, Tg_User
from log import logger
from telegram import (
    ReplyKeyboardMarkup, 
    Update, 
    InlineKeyboardButton, 
    InlineKeyboardMarkup
    )
from main.product_menu import conv_handler_prod

from telegram.constants import ParseMode
from admin.models import Category, Product


#from config import TOKEN
from telegram.ext import (
    Application,
    CommandHandler,
    ContextTypes,
    InlineQueryHandler,
    MessageHandler,
    CallbackQueryHandler,
    filters,
    PicklePersistence
)


async def start(update: Update, context: ContextTypes.DEFAULT_TYPE):
    """Запуск бота. Бот проверяет есть ли пользователь в базе, если нет, добавляет его в базу,
        Выводит клавитвуру для пользователя"""
    
    user = session.query(Tg_User).filter(Tg_User.id == update.message.chat.id).first()
    if not user:
        session.add(Tg_User(update.message.chat.id, update.message.chat.username, update.message.chat.first_name))
        session.commit()

    main_markup = InlineKeyboardMarkup(main_keyboard)

    await update.message.reply_text(
        "Главное меню",
        reply_markup=main_markup,
    )


async def main_menu(update: Update, context: ContextTypes.DEFAULT_TYPE):
    """Запуск бота. Бот проверяет есть ли пользователь в базе, если нет, добавляет его в базу,
        Выводит клавитвуру для пользователя"""
    
    query = update.callback_query

    await query.answer()

    main_markup = InlineKeyboardMarkup(main_keyboard)

    await query.edit_message_text(
        "Главное меню",
        reply_markup=main_markup,
    )


async def get_category(update: Update, context: ContextTypes.DEFAULT_TYPE):
    """Находит пользователя в базе и создает для него объект Cart"""
    if not context.user_data.get('cart'):
        context.user_data['cart'] = {}

    reply_markup = InlineKeyboardMarkup(category_list(context.user_data['cart']))

    query = update.callback_query
    await query.answer()
    prod_desc = await query.edit_message_text("Выберите категорию:", reply_markup=reply_markup)
    context.user_data['del_msg'] = prod_desc.message_id


async def cart_items(update: Update, context: ContextTypes.DEFAULT_TYPE):
    
    query = update.callback_query

    await query.answer()

    if update.callback_query.data != 'CART':
        context.user_data["cart"].pop(update.callback_query.data.split("_")[1])

    cart = context.user_data["cart"]

    keyboard = cart_keyboard(cart)

    reply_markup = InlineKeyboardMarkup(keyboard)

    del_msg = await query.edit_message_text(text="Содержимое корзины:", reply_markup=reply_markup)
    context.user_data["del_msg"] = del_msg.message_id

    


def start_bot() -> None:
    """Run the bot."""
    # Create the Application and pass it your bot's token.
    logger.info('Start telegram BOT')

    my_persistence = PicklePersistence(filepath='my_file')

    application = Application.builder().token(os.getenv("TELEGRAM_TOKEN")).persistence(persistence=my_persistence).build()

    application.add_handler(CommandHandler("start", start))

    application.add_handler(CallbackQueryHandler(main_menu, pattern="^" + "main_menu" + "$"))

    application.add_handler(CallbackQueryHandler(get_category, pattern="^" + "category" + "$"))

    application.add_handler(CallbackQueryHandler(get_category, pattern="^" + "RESET" + "$"))

    application.add_handler(conv_handler_event)

    application.add_handler(conv_handler_prod)

    application.add_handler(conv_handler_order)

    application.add_handler(CallbackQueryHandler(cart_items, pattern="^" + "CART" + "$"))

    application.add_handler(CallbackQueryHandler(cart_items, pattern="^" + "uhuhuhu_"))

    application.add_handler(InlineQueryHandler(inline_query_1))

    application.run_polling()









