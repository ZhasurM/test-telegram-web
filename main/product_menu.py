import os
from database import session
from log import logger
from main.keyboards import category_list, count_keybord
from telegram import (
    Update, 
    InlineKeyboardMarkup,
    )
    

from telegram.constants import ParseMode
from admin.models import Category, Product


#from config import TOKEN
from telegram.ext import (
    ContextTypes,
    ConversationHandler,
    MessageHandler,
    filters,
    CallbackQueryHandler
)

(
    START_ROUTES,
    STOP_ROUTES
) = range(2)
(
    INCREASE,
    DECREASE,
    CONFIRM,
    END_PRODUCT
) = range(4)


async def prod_descript(update: Update, context: ContextTypes.DEFAULT_TYPE):


    if not update.callback_query:
        text = update.message.text
        chat_id = update.message.chat_id
        await context.bot.delete_message(chat_id, update.message.message_id)
        try:
            await context.bot.delete_message(chat_id, context.user_data["del_msg"])
        except:
            pass
        text = text.replace('h7yf83f8gfrv098owpow0e08u', '')
        try:
            context.user_data['count'] = context.user_data["cart"][text]
        except:
            context.user_data['count'] = 1
        
    else:
        text = update.callback_query.data
        text = text.replace('h7yf83f8gfrv098owpow0e08u', '')
        query = update.callback_query
        chat_id = update.callback_query.message.chat.id
        await query.answer()
        context.user_data['count'] = int(context.user_data["cart"][text])


    prod = session.query(Product).filter(Product.name == text).first()
    context.user_data['product'] = prod

    reply_markup = count_keybord(context.user_data['count'])
    

    caption = f"""<b>{prod.name}</b>\n{prod.description}\n{context.user_data['count'] * prod.price} ₸"""

    prod_desc = await context.bot.sendPhoto(chat_id=chat_id, 
                                            photo=os.getenv('PHOTO_URL')+prod.image, 
                                            caption=caption, parse_mode=ParseMode.HTML,
                                            reply_markup=reply_markup)
    context.user_data['del_msg'] = prod_desc.message_id
    return START_ROUTES

async def increase(update: Update, context: ContextTypes.DEFAULT_TYPE):
    prod = context.user_data['product']
    context.user_data['count'] = context.user_data['count'] + 1
    count = context.user_data['count']

    query = update.callback_query
    await query.answer()
    await context.bot.delete_message(query.message.chat_id, context.user_data['del_msg'])
    reply_markup = count_keybord(count)
    caption = f"""<b>{prod.name}</b>\n{prod.description}\n{prod.price*count} ₸"""
    prod_desc = await context.bot.sendPhoto(chat_id=query.message.chat.id, 
                                            photo=os.getenv('PHOTO_URL')+prod.image, 
                                            caption=caption, parse_mode=ParseMode.HTML,
                                            reply_markup=reply_markup)
    context.user_data['del_msg'] = prod_desc.message_id
    return START_ROUTES


async def decrease(update: Update, context: ContextTypes.DEFAULT_TYPE):

    prod = context.user_data['product']
    
    if context.user_data['count'] > 0:
        context.user_data['count'] = context.user_data['count'] - 1
    count = context.user_data['count']

    query = update.callback_query
    await query.answer()

    await context.bot.delete_message(query.message.chat_id, context.user_data['del_msg'])

    reply_markup = count_keybord(count)
    caption = f"""<b>{prod.name}</b>\n{prod.description}\n{prod.price*count} ₸"""
    prod_desc = await context.bot.sendPhoto(chat_id=query.message.chat.id, 
                                            photo=os.getenv('PHOTO_URL')+prod.image, 
                                            caption=caption, parse_mode=ParseMode.HTML,
                                            reply_markup=reply_markup)
    context.user_data['del_msg'] = prod_desc.message_id
    return START_ROUTES


async def confirm_prod(update: Update, context: ContextTypes.DEFAULT_TYPE):
    prod = context.user_data['product']
    count = context.user_data['count']

    context.user_data["cart"][prod.name] = count
    reply_markup = InlineKeyboardMarkup(category_list(context.user_data['cart']))

    query = update.callback_query
    await context.bot.delete_message(query.message.chat_id, context.user_data['del_msg'])
    await query.answer()
    del_msg = await query.message.reply_text("Выберите категорию:", reply_markup=reply_markup)
    context.user_data["del_msg"] = del_msg.message_id
    return ConversationHandler.END


async def end_product(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Handle the inline query. This is run when you type: @botusername <query>"""
    reply_markup = InlineKeyboardMarkup(category_list(context.user_data['cart']))

    query = update.callback_query
    await query.answer()
    await context.bot.delete_message(query.message.chat_id, context.user_data['del_msg'])
    del_msg = await query.message.reply_text("Выберите категорию:", reply_markup=reply_markup)
    context.user_data["del_msg"] = del_msg.message_id
    return ConversationHandler.END


handlers = [MessageHandler(filters.Regex("h7yf83f8gfrv098owpow0e08u$"), prod_descript)]
for i in session.query(Product).all():
    handlers.append(CallbackQueryHandler(prod_descript, pattern="^" + str(i.name) + "$"))

conv_handler_prod = ConversationHandler(
    entry_points =[MessageHandler(filters.Regex("h7yf83f8gfrv098owpow0e08u$"), prod_descript),
                    CallbackQueryHandler(prod_descript, pattern="^"+"h7yf83f8gfrv098owpow0e08u")],
    states = {
        START_ROUTES: [
            CallbackQueryHandler(increase, pattern="^" + str(INCREASE) + "$"),
            CallbackQueryHandler(decrease, pattern="^" + str(DECREASE) + "$"),
            CallbackQueryHandler(confirm_prod, pattern="^" + str(CONFIRM) + "$")
        ],
        STOP_ROUTES: [
            CallbackQueryHandler(end_product, pattern="^" + str(END_PRODUCT) + "$")
        ]
    },
    allow_reentry = True,
    #per_user=True,
    name = "product",
    persistent = True,
    fallbacks = [CallbackQueryHandler(end_product, pattern="^" + str(END_PRODUCT) + "$"),
                CallbackQueryHandler(confirm_prod, pattern="^" + str(CONFIRM) + "$")]
)