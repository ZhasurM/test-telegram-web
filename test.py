import requests

data = {
    "grant_type": "client_credentials",
    "scope": "payment",
    "client_id": "test",
    "client_secret": "yF587AV9Ms94qN2QShFzVR3vFnWkhjbAK3sG",
    "invoiceID": "000000001",
    "amount": 100,
    "currency": "KZT",
    "terminal": "67e34d63-102f-4bd1-898e-370781d0074d",
    "postLink":"",
    "failurePostLink": ""
}

r = requests.post('https://testoauth.homebank.kz/epay2/oauth2/token', data = data)

print(r.text)